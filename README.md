# Données géo et R : de la donnée à la carte
* Place : Meetup R Nantes
* Author: Nicolas Roelandt
* date: "jeudi 28 février 2019

La présentation au format HTML est disponible en cliquant sur ce [lien](https://roelandtn.frama.io/20190228_meetup_r_data_geo/#1)


This presentation is in [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode). Illustrations used in it are property of their respective authors.